TEXMF_DIR=$(HOME)/texmf
LATEX_DIR=$(TEXMF_DIR)/tex/latex/local/latex
BEAMER_THEMES_DIR=$(LATEX_DIR)/beamer/themes
TEXHASH=texhash

install:
	mkdir -p $(LATEX_DIR)/camp
	mkdir -p $(BEAMER_THEMES_DIR)/color
	mkdir -p $(BEAMER_THEMES_DIR)/inner
	mkdir -p $(BEAMER_THEMES_DIR)/outer
	mkdir -p $(BEAMER_THEMES_DIR)/theme
	cp camp.sty campcolors.sty camplogos.sty proposal/campProposal.cls $(LATEX_DIR)/camp
	cp beamer/themes/color/beamercolorthemetum.sty $(BEAMER_THEMES_DIR)/color/
	cp beamer/themes/inner/beamerinnerthemefancy.sty beamer/themes/inner/CAMP_logo_transparent.pdf $(BEAMER_THEMES_DIR)/inner/
	cp beamer/themes/outer/beamerouterthemecamp.sty beamer/themes/outer/CAMP_logo_2color.pdf $(BEAMER_THEMES_DIR)/outer/
	cp beamer/themes/theme/beamerthemeCamp.sty $(BEAMER_THEMES_DIR)/theme/
	chmod -R a+r $(LATEX_DIR)/camp
	chmod -R a+r $(BEAMER_THEMES_DIR)
	$(TEXHASH) $(TEXMF_DIR)
