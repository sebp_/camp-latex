\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{campProposal}[2012/11/12 - camp proposal template]

% Load the Base Class
\LoadClassWithOptions{article}

% Begin Requirements
\RequirePackage{camp}
\RequirePackage{authblk}

%%%%%
%% settings
%%%

% page margins
\RequirePackage[top=2cm,left=2.54cm,right=2.54cm,bottom=2cm]{geometry}

% background image
\definecolor{backgroundlogocolor}{HTML}{C6E0F3}
\setwallpaper{\camplogo[backgroundlogocolor]{15cm}}{-7.75cm}{-10.2cm}

% set some lengths
\newlength{\headerlogosize}
\setlength{\headerlogosize}{1cm}

% text color
\RequirePackage{titlesec}
\titleformat{\section}{\color{camptextcolor}\normalfont\Large\bfseries}{\thesection}{1em}{}
\titleformat{\subsection}{\color{camptextcolor}\normalfont\large\bfseries}{\thesubsection}{1em}{}
\titleformat{\subsubsection}{\color{camptextcolor}\normalfont\normalsize\bfseries}{\thesubsubsection}{1em}{}
\titleformat{\paragraph}[runin]{\color{camptextcolor}\normalfont\normalsize\bfseries}{\theparagraph}{1em}{}
\titleformat{\subparagraph}[runin]{\color{camptextcolor}\normalfont\normalsize\bfseries}{\theparagraph}{1em}{}

% paragraph spacing
\titlespacing{\paragraph}{%
  0pt}{%              left margin
  0.5\baselineskip}{% space before (vertical)
  1em}%               space after (horizontal)

% add option subtitle
\def\@hassubtitle{\empty}
\newcommand*{\subtitle}[1]{
\def\@subtitle{#1}
\def\@hassubtitle{true}}


%%%%%
%% custom commands
%%%

%
% Name:		\maketitle
%
% Syntax:	\maketitle
%
% Description: overriding the standard maketitle for camp purpose
%
\renewcommand{\maketitle}[0]
{
\AddToShipoutPicture*{
    \AtPageLowerLeft{%
      \parbox[b][\paperheight]{\paperwidth}{%
        \vspace{\headerlogosize}
        \hfill \tumlogo{1.2\headerlogosize}
        \hspace{\headerlogosize}
        \vfill%
      }}
      \AtPageLowerLeft{%
      \parbox[b][\paperheight]{\paperwidth}{
        \vspace{\headerlogosize}
        \hspace{\headerlogosize}
        \camplogo{.8\headerlogosize}\\[-0.5\headerlogosize]
        %\hspace*{2.5\headerlogosize}\hspace*{1mm}\tiny{\textcolor{camptextcolor}{Chair for Computer Aided Medical}}\\
        %\hspace*{2.5\headerlogosize}\hspace*{1mm}\tiny{\textcolor{camptextcolor}{Procedures \& Augmented Reality}}%
        \vfill%
      }
    }
  }


\vspace*{0cm}
\begin{center}
\Huge{\textbf{\@title}}
\ifthenelse{ \equal{\@hassubtitle}{\empty} }{}{\\[5mm]\Large{\@subtitle}}
\\[5mm]\large{\@author}
\vspace*{1.5cm}
\end{center}

}
