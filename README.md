# CAMP LaTeX Templates

This repository contains a [Beamer](http://latex-beamer.sourceforge.net/)
theme with CAMP branding and colors. The theme is based on Marco Barisione's
[Torino theme](http://blog.barisione.org/2007-09/torino-a-pretty-theme-for-latex-beamer/).

## Installation

On Linux:
```
make install
```

Now you are ready to use the theme, for documentation read the example
file.
```
pdflatex beamer/campthemeexample.tex
```
